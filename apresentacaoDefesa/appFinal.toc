\beamer@endinputifotherversion {3.36pt}
\select@language {brazil}
\beamer@sectionintoc {1}{Roteiro da apresenta\IeC {\c c}\IeC {\~a}o}{2}{0}{1}
\beamer@sectionintoc {3}{Introdu\IeC {\c c}\IeC {\~a}o}{3}{0}{3}
\beamer@sectionintoc {4}{Introdu\IeC {\c c}\IeC {\~a}o ferramentas}{5}{0}{4}
\beamer@sectionintoc {5}{Introdu\IeC {\c c}\IeC {\~a}o processo ScriptLattes}{6}{0}{5}
\beamer@sectionintoc {6}{Introdu\IeC {\c c}\IeC {\~a}o trabalho}{7}{0}{6}
\beamer@sectionintoc {2}{introducao}{3}{0}{2}
\beamer@subsectionintoc {7}{1}{Questoes}{8}{0}{7}
\beamer@sectionintoc {7}{problema}{8}{0}{7}
\beamer@sectionintoc {9}{Aplica\IeC {\c c}\IeC {\~o}es - Hierarquia de curr\IeC {\'\i }culos}{14}{0}{9}
\beamer@sectionintoc {10}{Aplica\IeC {\c c}\IeC {\~o}es - Aplica\IeC {\c c}\IeC {\~a}o para cadastro da plataforma Sucupiras}{17}{0}{10}
\beamer@sectionintoc {11}{Aplica\IeC {\c c}\IeC {\~o}es - Aplica\IeC {\c c}\IeC {\~a}o para cadastro da plataforma e-Mec}{21}{0}{11}
\beamer@sectionintoc {12}{Aplica\IeC {\c c}\IeC {\~o}es - ScriptComp}{36}{0}{12}
\beamer@sectionintoc {13}{IndProdArt}{39}{0}{13}
\beamer@sectionintoc {14}{IndProdArtSUP}{41}{0}{14}
\beamer@sectionintoc {15}{IndProdOrient}{43}{0}{15}
\beamer@sectionintoc {16}{IndProd}{45}{0}{16}
\beamer@sectionintoc {8}{agrupamento}{12}{0}{8}
\beamer@sectionintoc {18}{conclusao}{47}{0}{18}
\beamer@sectionintoc {17}{conclusao}{47}{0}{17}
