\beamer@endinputifotherversion {3.36pt}
\select@language {brazil}
\beamer@sectionintoc {1}{Roteiro da apresenta\IeC {\c c}\IeC {\~a}o}{2}{0}{1}
\beamer@sectionintoc {3}{Introdu\IeC {\c c}\IeC {\~a}o}{3}{0}{3}
\beamer@sectionintoc {4}{Introdu\IeC {\c c}\IeC {\~a}o ferramentas}{5}{0}{4}
\beamer@sectionintoc {5}{Introdu\IeC {\c c}\IeC {\~a}o processo ScriptLattes}{6}{0}{5}
\beamer@sectionintoc {6}{Introdu\IeC {\c c}\IeC {\~a}o trabalho}{7}{0}{6}
\beamer@sectionintoc {2}{introducao}{3}{0}{2}
\beamer@subsectionintoc {7}{1}{Questoes}{8}{0}{7}
\beamer@sectionintoc {7}{problema}{8}{0}{7}
\beamer@sectionintoc {9}{Aplica\IeC {\c c}\IeC {\~o}es - Hierarquia de curr\IeC {\'\i }culos}{14}{0}{9}
\beamer@sectionintoc {10}{Aplica\IeC {\c c}\IeC {\~o}es - Aplica\IeC {\c c}\IeC {\~a}o para cadastro da plataforma Sucupiras}{17}{0}{10}
\beamer@sectionintoc {11}{Aplica\IeC {\c c}\IeC {\~o}es - Aplica\IeC {\c c}\IeC {\~a}o para cadastro da plataforma e-Mec}{21}{0}{11}
\beamer@sectionintoc {12}{Aplica\IeC {\c c}\IeC {\~o}es - ScriptComp}{36}{0}{12}
\beamer@sectionintoc {13}{IndProdArt}{39}{0}{13}
\beamer@sectionintoc {14}{IndProdArtSUP}{41}{0}{14}
\beamer@sectionintoc {15}{IndProdOrient}{43}{0}{15}
\beamer@sectionintoc {16}{IndProd}{45}{0}{16}
\beamer@sectionintoc {8}{agrupamento}{12}{0}{8}
\beamer@sectionintoc {18}{Aplica\IeC {\c c}\IeC {\~o}es - Hierarquia de curr\IeC {\'\i }culos}{47}{0}{18}
\beamer@sectionintoc {19}{Aplica\IeC {\c c}\IeC {\~o}es - Aplica\IeC {\c c}\IeC {\~a}o para cadastro da plataforma Sucupiras}{50}{0}{19}
\beamer@sectionintoc {20}{Aplica\IeC {\c c}\IeC {\~o}es - Aplica\IeC {\c c}\IeC {\~a}o para cadastro da plataforma e-Mec}{56}{0}{20}
\beamer@sectionintoc {21}{Aplica\IeC {\c c}\IeC {\~o}es - ScriptComp}{62}{0}{21}
\beamer@sectionintoc {22}{IndProdArt}{65}{0}{22}
\beamer@sectionintoc {23}{IndProdArtSUP}{67}{0}{23}
\beamer@sectionintoc {24}{IndProdOrient}{69}{0}{24}
\beamer@sectionintoc {25}{IndProd}{71}{0}{25}
\beamer@sectionintoc {17}{aplicacoes}{47}{0}{17}
\beamer@sectionintoc {27}{conclusao}{73}{0}{27}
\beamer@sectionintoc {26}{conclusao}{73}{0}{26}
\beamer@sectionintoc {28}{resultados}{77}{0}{28}
